package com.homework.github.ui;

import android.content.Intent;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.homework.github.R;
import com.homework.github.model.Repo;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasChildCount;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
public class SearchActivityTest {

    private static final String TEST_NAME = "test 1";
    private static final String TEST_DESC = "desc 1";
    private static final String TEST_NAME2 = "test 2";
    private static final String TEST_DESC2 = "desc 2";
    private static final String TEST_DATE = "2019-03-16T17:22:12Z";
    private static final String TEST_DATE2 = "2019-03-15T17:22:12Z";
    private static final int TEST_FORKS = 1;
    private static final int TEST_STARS = 2;

    @Rule
    public ActivityTestRule<SearchActivity> activityActivityTestRule =
            new ActivityTestRule<>(SearchActivity.class, false, false);

    public SearchActivity activity;

    @Before
    public void init() {
        activity = activityActivityTestRule.launchActivity(new Intent());

    }

    @Test
    public void toggleUserData() throws Throwable {
        activityActivityTestRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.toggleUserData(true);
            }
        });

        onView(withId(R.id.ll_profile)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
        //onView(withId(R.id.rv_repos)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));

        activityActivityTestRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.toggleUserData(false);
            }
        });

        onView(withId(R.id.ll_profile)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)));
        //onView(withId(R.id.rv_repos)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)));
    }

    @Test
    public void loadData() throws Throwable {
        activityActivityTestRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.githubUserViewModel.addReposToAdapter(prepareList());
            }
        });

        onView(withId(R.id.rv_repos)).check(matches(hasChildCount(2)));
        onView(withText(TEST_NAME)).check(matches(isDescendantOfA(withId(R.id.rv_repos))));
        onView(withText(TEST_DESC)).check(matches(isDescendantOfA(withId(R.id.rv_repos))));
        onView(withText(TEST_NAME2)).check(matches(isDescendantOfA(withId(R.id.rv_repos))));
        onView(withText(TEST_DESC2)).check(matches(isDescendantOfA(withId(R.id.rv_repos))));
    }

    private List<Repo> prepareList() {
        List<Repo> repos = new ArrayList<>();

        Repo item1 = new Repo();
        item1.setName(TEST_NAME);
        item1.setDescription(TEST_DESC);
        item1.setForks(TEST_FORKS);
        item1.setStargazersCount(TEST_STARS);
        item1.setUpdatedAt(TEST_DATE);

        Repo item2 = new Repo();
        item2.setName(TEST_NAME2);
        item2.setDescription(TEST_DESC2);
        item2.setForks(TEST_FORKS);
        item2.setStargazersCount(TEST_STARS);
        item2.setUpdatedAt(TEST_DATE2);

        repos.add(item1);
        repos.add(item2);

        return repos;
    }
}
