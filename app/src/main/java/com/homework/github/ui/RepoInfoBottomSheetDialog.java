package com.homework.github.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.homework.github.R;
import com.homework.github.databinding.RepoInfoBinding;
import com.homework.github.model.Repo;

public class RepoInfoBottomSheetDialog extends BottomSheetDialogFragment {

    public static final String PARAM_REPO = "repo";

    public RepoInfoBottomSheetDialog() {
        setRetainInstance(true);
    }

    public static RepoInfoBottomSheetDialog newInstance(Repo repoInfo) {
        RepoInfoBottomSheetDialog dialogFragment = new
                RepoInfoBottomSheetDialog();
        Bundle args = new Bundle();
        args.putParcelable(PARAM_REPO, repoInfo);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.RepoInfoBottomSheetDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.repo_info, container);
        RepoInfoBinding binding = RepoInfoBinding.bind(view);
        if (getArguments() != null) {
            binding.setRepo((Repo) getArguments().getParcelable(PARAM_REPO));
        }
        return binding.getRoot();
    }
}
