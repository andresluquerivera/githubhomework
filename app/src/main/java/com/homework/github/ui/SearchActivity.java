package com.homework.github.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.homework.github.R;
import com.homework.github.databinding.ActivitySearchBinding;
import com.homework.github.model.Repo;
import com.homework.github.util.Util;
import com.homework.github.viewmodel.GithubUserViewModel;

import java.util.List;

public class SearchActivity extends AppCompatActivity implements TextView.OnEditorActionListener {

    private static final String DIALOG_KEY = "bottom_dialog_fragment";

    protected GithubUserViewModel githubUserViewModel;
    private LinearLayout llProfile;
    private RecyclerView rvRepos;

    private Animation slideUpFadeinProfile;
    private Animation slideUpFadeinRepos;
    private Animation slideDownFadeOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setupBindings(savedInstanceState);
        setup();
    }


    private void setupBindings(Bundle savedInstanceState) {
        ActivitySearchBinding activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        githubUserViewModel = ViewModelProviders.of(this).get(GithubUserViewModel.class);
        if (savedInstanceState == null) {
            githubUserViewModel.init();
        }
        activityBinding.setViewModel(githubUserViewModel);
        activityBinding.setUser(githubUserViewModel.getUser());
        activityBinding.edtUserid.setOnEditorActionListener(this);

        llProfile = activityBinding.llProfile;
        rvRepos = activityBinding.rvRepos;
    }

    private void setup() {
        // Create animation
        slideUpFadeinProfile = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slideUpFadeinRepos = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        slideDownFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        slideUpFadeinProfile.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // do nothing
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rvRepos.setAnimation(slideUpFadeinRepos);
                githubUserViewModel.isLoading(false);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // do nothing
            }
        });

        // setup data changes listeners
        githubUserViewModel.getUser().addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                SearchActivity.this.toggleUserData(true);
            }
        });
        githubUserViewModel.getRepos().observe(this, new Observer<List<Repo>>() {
            @Override
            public void onChanged(List<Repo> repos) {
                if (repos == null) {
                    Toast.makeText(SearchActivity.this, R.string.not_found, Toast.LENGTH_LONG).show();
                    SearchActivity.this.toggleUserData(false);
                }
                githubUserViewModel.addReposToAdapter(repos);
            }
        });

        githubUserViewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    Util.hideSoftKeyboard(SearchActivity.this, llProfile);
                }
            }
        });
        setupListClick();
    }


    public void toggleUserData(boolean display) {
        if (display) {
            llProfile.startAnimation(slideUpFadeinProfile);
        } else {
            llProfile.startAnimation(slideDownFadeOut);
            rvRepos.startAnimation(slideDownFadeOut);
            llProfile.setVisibility(View.INVISIBLE);
            rvRepos.setVisibility(View.INVISIBLE);
        }
    }

    private void setupListClick() {
        githubUserViewModel.getSelected().observe(this, new Observer<Repo>() {
            @Override
            public void onChanged(Repo repoInfo) {
                RepoInfoBottomSheetDialog repoInfoBottomSheetDialog =
                        RepoInfoBottomSheetDialog.newInstance(repoInfo);
                repoInfoBottomSheetDialog.show(getSupportFragmentManager(),
                        DIALOG_KEY);
            }
        });
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            githubUserViewModel.fetchData();
            Util.hideSoftKeyboard(this, v);
            return true;
        }
        return false;
    }
}
