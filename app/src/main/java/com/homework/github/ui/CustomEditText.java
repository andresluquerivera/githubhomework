package com.homework.github.ui;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.inputmethod.InputMethodManager;

import com.homework.github.R;

public class CustomEditText extends AppCompatEditText {

    private static final String EMPTY_HINT  = "";

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        setHint(focused ? EMPTY_HINT : getContext().getString(R.string.hint_userid));
        if (!focused) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getWindowToken(), 0);
        }
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
    }
}
