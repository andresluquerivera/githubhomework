package com.homework.github.net;

import com.homework.github.model.Repo;
import com.homework.github.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {

    @GET("/users/{userId}")
    Call<User> getUser(@Path("userId") String userId);

    @GET("/users/{userId}/repos")
    Call<List<Repo>> getRepos(@Path("userId") String userId);
}
