package com.homework.github.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    private static final String INPUT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final String OUTPUT_FORMAT = "MMM dd, yyyy hh:mm:ss a";

    public static String formatDate(String unformattedDate) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(INPUT_FORMAT);
        final Date date = simpleDateFormat.parse(unformattedDate);
        simpleDateFormat.applyPattern(OUTPUT_FORMAT);
        return simpleDateFormat.format(date);
    }

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
