package com.homework.github.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.homework.github.R;
import com.homework.github.adapter.ReposAdapter;
import com.homework.github.model.Repo;
import com.homework.github.model.Repos;
import com.homework.github.model.User;

import java.util.List;

public class GithubUserViewModel extends ViewModel {

    private String userId;
    private Repos repos;
    private User user;
    private ReposAdapter adapter;
    private MutableLiveData<Repo> selectedRepo;
    private MutableLiveData<Boolean> loading;

    public void init() {
        adapter = new ReposAdapter(R.layout.repo_item, this);
        repos = new Repos();
        user = new User();
        selectedRepo = new MutableLiveData<>();
        loading = new MutableLiveData<>();
    }

    public void fetchData() {
        isLoading(true);
        if (userId != null) {
            user.fetchUser(userId);
            repos.fetchRepos(userId);
        }
    }

    public MutableLiveData<Boolean> isLoading() {
        return loading;
    }

    public void isLoading(boolean isLoading) {
        loading.postValue(isLoading);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public MutableLiveData<List<Repo>> getRepos() {
        return repos.getRepos();
    }

    public ReposAdapter getAdapter() {
        return adapter;
    }

    public void addReposToAdapter(List<Repo> repos) {
        this.adapter.setRepos(repos);
        this.adapter.notifyDataSetChanged();
    }

    public MutableLiveData<Repo> getSelected() {
        return selectedRepo;
    }

    public void onItemClick(Repo repo) {
        selectedRepo.setValue(repo);
    }
}
