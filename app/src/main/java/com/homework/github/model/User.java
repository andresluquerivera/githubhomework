package com.homework.github.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;
import com.homework.github.BR;
import com.homework.github.net.Api;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class User extends BaseObservable {

    private String name;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Bindable
    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public void fetchUser(String userId) {
        Callback<User> callback = new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User userResponse = response.body();
                if (userResponse != null) {
                    name = userResponse.name;
                    avatarUrl = userResponse.avatarUrl;
                    notifyPropertyChanged(BR.name);
                    notifyPropertyChanged(BR.avatarUrl);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                // Catch failure
            }
        };

        Api.getApi().getUser(userId).enqueue(callback);
    }

}
