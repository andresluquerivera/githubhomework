package com.homework.github.model;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.BaseObservable;
import android.util.Log;

import com.homework.github.net.Api;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repos extends BaseObservable {

    private List<Repo> repoList = new ArrayList<>();
    private MutableLiveData<List<Repo>> repos = new MutableLiveData<>();

    public List<Repo> getRepoList() {
        return repoList;
    }

    public void setRepoList(List<Repo> repoList) {
        this.repoList = repoList;
    }

    public MutableLiveData<List<Repo>> getRepos() {
        return repos;
    }

    public void setRepos(MutableLiveData<List<Repo>> repos) {
        this.repos = repos;
    }

    public void fetchRepos(String userId) {
        Callback<List<Repo>> callback = new Callback<List<Repo>>() {
            @Override
            public void onResponse(Call<List<Repo>> call, Response<List<Repo>> response) {
                List<Repo> reposResponse = response.body();
                repoList = reposResponse;
                repos.setValue(repoList);
            }

            @Override
            public void onFailure(Call<List<Repo>> call, Throwable t) {
                Log.e("Test", t.getMessage(), t);
            }
        };

        Api.getApi().getRepos(userId).enqueue(callback);
    }
}
