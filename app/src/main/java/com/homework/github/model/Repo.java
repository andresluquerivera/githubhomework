package com.homework.github.model;

import android.databinding.BaseObservable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.homework.github.util.Util;

import java.text.ParseException;

public class Repo extends BaseObservable implements Parcelable {

    public static final Creator<Repo> CREATOR = new Creator<Repo>() {
        @Override
        public Repo createFromParcel(Parcel in) {
            return new Repo(in);
        }

        @Override
        public Repo[] newArray(int size) {
            return new Repo[size];
        }
    };

    private String name;
    private String description;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("stargazers_count")
    private int stargazersCount;

    private int forks;

    protected Repo(Parcel in) {
        name = in.readString();
        description = in.readString();
        updatedAt = in.readString();
        stargazersCount = in.readInt();
        forks = in.readInt();
    }

    public Repo() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdatedAt() {
        try {
            return Util.formatDate(updatedAt);
        } catch (ParseException e) {
            return updatedAt;
        }
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(int stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(updatedAt);
        dest.writeInt(stargazersCount);
        dest.writeInt(forks);
    }
}
