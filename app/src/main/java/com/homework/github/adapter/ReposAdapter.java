package com.homework.github.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.homework.github.BR;
import com.homework.github.model.Repo;
import com.homework.github.viewmodel.GithubUserViewModel;

import java.util.List;

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.GenericViewHolder> {

    private int layoutId;
    private List<Repo> repos;
    private GithubUserViewModel viewModel;

    public ReposAdapter(@LayoutRes int layoutId, GithubUserViewModel viewModel) {
        this.layoutId = layoutId;
        this.viewModel = viewModel;
    }


    public Repo getRepoAt(int position) {
        return repos.get(position);
    }


    private int getLayoutIdForPosition(int position) {
        return layoutId;
    }

    @Override
    public int getItemCount() {
        return repos == null ? 0 : repos.size();
    }

    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);

        return new GenericViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GenericViewHolder holder, int position) {
        Repo obj = getRepoAt(position);
        holder.bind(obj, viewModel);
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    public void setRepos(List<Repo> breeds) {
        this.repos = breeds;
    }


    class GenericViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding binding;

        GenericViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Repo repo, GithubUserViewModel viewModel) {
            binding.setVariable(BR.repo, repo);
            binding.setVariable(BR.viewModel, viewModel);
            binding.executePendingBindings();
        }

    }
}