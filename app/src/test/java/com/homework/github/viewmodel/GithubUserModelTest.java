package com.homework.github.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import com.homework.github.model.Repo;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class GithubUserModelTest {

    private Repo item1 = Mockito.mock(Repo.class);
    private Repo item2 = Mockito.mock(Repo.class);

    @Mock
    Observer<Boolean> loadingObserver;
    @Mock
    Observer<List<Repo>> reposObserver;

    GithubUserViewModel viewModel;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void init() {
        viewModel = new GithubUserViewModel();
        viewModel.init();
    }

    @Test
    public void isLoading() {
        viewModel.isLoading().observeForever(loadingObserver);

        // test loading
        viewModel.isLoading(true);
        Mockito.verify(loadingObserver).onChanged(true);

        // test not loading
        viewModel.isLoading(false);
        Mockito.verify(loadingObserver).onChanged(false);
    }

    @Test
    public void getRepos() {
        viewModel.getRepos().observeForever(reposObserver);

        List<Repo> repos = new ArrayList<>();
        repos.add(Mockito.mock(Repo.class));
        repos.add(Mockito.mock(Repo.class));
        viewModel.getRepos().postValue(repos);
        Mockito.verify(reposObserver).onChanged(repos);
    }
}
