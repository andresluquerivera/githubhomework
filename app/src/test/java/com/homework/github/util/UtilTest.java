package com.homework.github.util;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;

public class UtilTest {

    @Test
    public void formatDate() {
        final String inputDate = "2019-03-16T17:22:12Z";
        final String outputDate = "mar 16, 2019 05:22:12 PM";

        try {
            Assert.assertEquals(outputDate, Util.formatDate(inputDate));
        } catch (ParseException e) {

        }
    }
}
