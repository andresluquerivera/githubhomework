package com.homework.github.net;

import com.homework.github.model.Repo;
import com.homework.github.model.User;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class ApiTest {

    private static final String TEST_ID = "octocat";

    @Test
    public void getUser() {

        ApiInterface apiEndpoints = Api.getApi();

        Call<User> call = apiEndpoints.getUser(TEST_ID);

        try {
            //Magic is here at .execute() instead of .enqueue()
            Response<User> response = call.execute();
            User user = response.body();

            Assert.assertTrue(response.isSuccessful() && user != null);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getRepos() {

        ApiInterface apiEndpoints = Api.getApi();

        Call<List<Repo>> call = apiEndpoints.getRepos(TEST_ID);

        try {
            //Magic is here at .execute() instead of .enqueue()
            Response<List<Repo>> response = call.execute();
            List<Repo> repos = response.body();

            Assert.assertTrue(response.isSuccessful() && repos != null);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
